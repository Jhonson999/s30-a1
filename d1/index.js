	const express = require('express');
	const mongoose = require('mongoose');
	const port = 4000;

	const app = express();

	app.use(express.json());
	app.use(express.urlencoded({extended: true}));




	//Syntax:
	// mongoose.connect('<connection string>. {middlewares')

	mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.mbqhe.mongodb.net/session30?retryWrites=true&w=majority',

	{

		useNewUrlParser: true,
		useUnifiedTopology: true
	}

	);

	let db = mongoose.connection;

	// console.error.bind(console)-print error in the browser and in the terminal
	db.on("error", console.error.bind(console, "Connection Error"))

	// if connection is successful, this will be the output in our console
	db.once('open',() => console.log('Connected to the cloud database'))

	// Mongoose Schema

	const taskSchema = new mongoose.Schema({

		//define the name of our shema - taskSchema
		//new mongoose.Schema method-to make a schema
		// we will be needing the name of the task and its status
		//each field will require a data type

		name: String,
		status: {
			type: String,
			default: 'pending'
		}
	});


	const Task = mongoose.model('Task', taskSchema); 
	   // models use schemas and they act as the middleman from the server to our database
	   // Model can now be used to run commands for interacting with our database
	   // naming convention - name of model should be capitalized and singular form - 'Task'
	   // second paramtere is used to specify the schema of the documents that will be stored in the mongoDB collection

	// Business Logic
	/*

	 1. Add a functionality to check if there are duplicate tasks
	     -if the task already exists in the db, we return an error
	     -if the task doesn't exists in the db, we add in inthe db.
	 2. The task data will be coming from the request body.
	 3. Create a new Task object with name field property.

	*/

	app.post('/tasks', (req, res) => {

		// check any duplucate task
		//err shorthand of error
		Task.findOne({name: req.body.name}, (err, result) =>{
	        
	        // if there are matches, 
			if(result != null && result.name === req.body.name){
	            
	            // will retrun this message
				return res.send('Duplicate task found.')

		    }else{

		    	let newTask = new Task({

		    		name: req.body.name
		    	})

	            // save method will accept a callback function which stores any errors in the first parameter and will store the newly saved document in the second parameter
		    	newTask.save((saveErr, savedTask) => {
	                
	                // if there are any errors, it will print the error
		    		if(saveErr){
		    			return console.error(saveErr)
	                //no error found, return tha status code and the message
		    	    }else{

		    	    	return res.status(201).send('New Task Created')
		    		}
		    	})
			}
		})
	})

	//Retrieving all tasks

	app.get('/tasks', (req, res) => {
		Task.find({}, (err, result) =>{

			if(err){

				return console.log(err);
			}else{
				return res.status(200).json({
					data:result
				})

		    }
		})
	})

	//ACTIVITY:


    // 1.) Create a User schema.
    // 	<username> : String,
    // 	<password> :String

	const userSchema = new mongoose.Schema({

		username: String,
		password: String

	});
    // 2.) Create a User model.
	const User = mongoose.model('User', userSchema); 
    // 3.) Create a POST route to register 3 users.
	// 4.) Name the endpoint /signup
 //        Business Logic:
	//  Add a functionality to check if there are duplicate username
	//      -if the user already exists in the database, we return an error
	//      -if the user doesn't exist in the database, we add it in the database.
	//   The user data will be coming from the request's body.
	//   Create a new User object with a "username" and "password" fields/properties

	app.post('/signup', (req, res) => {


		User.findOne({username: req.body.username}, (err, result) =>{
	        
	        
			if(result != null && result.username === req.body.username){
	            
	           
				return res.send('Error. Username already used.')

		    }else{

		    	let newUser = new User({

		    		username: req.body.username,
		    		password: req.body.password
		    	})

	            
		    	newUser.save((saveErr, savedUser) => {
	                
	                
		    		if(saveErr){
		    			return console.error(saveErr)
	                
		    	    }else{

		    	    	return res.status(201).send('New User Created')
		    		}
		    	})
			}
		})
	})
    // 5.) Retrieve all users that registered.
	app.get('/users', (req, res) => {
		User.find({}, (err, result) =>{

			if(err){

				return console.log(err);
			}else{
				return res.status(200).json({
					data:result
				})

		    }
		})
	})


	app.listen(port, () => console.log(`Server is running at port ${port}`))

			 
